package com.knoblymedia.wifiswitcher;

import android.app.Activity;
import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.knoblymedia.wifiswitcher.util.ConnectionDetector;


public class MainActivity extends Activity {
    Handler handler;
    EditText onduration,offduration;
    Button submit;
    int getdataon,getdataoff;
    String offdata1,ondata1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        xmlmapping();
        this.handler=new Handler();


        submit= (Button) findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager inputManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);

                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);
                String ondata=onduration.getText().toString();
                Toast.makeText(getApplicationContext(),"new duration settings made",Toast.LENGTH_SHORT).show();
                 ondata1=ondata.trim();
                String offdata=offduration.getText().toString();
                offdata1=offdata.trim();



                handler.postDelayed(m_Runnable,(Integer.parseInt(offdata1))*1000);

            }
        });



//        on=Integer.parseInt(ondata1);
//        off=Integer.parseInt(offdata1);
    }

    private void xmlmapping() {
        onduration= (EditText) findViewById(R.id.wifionduration);
        offduration= (EditText) findViewById(R.id.wifiofduration);
    }

    private final Runnable m_Runnable = new Runnable()
    {
        public void run()

        {
      toggleWiFi(false);
            if(wificonnection())
            {
                Toast.makeText(getApplicationContext(), "wifi is  off", Toast.LENGTH_LONG).show();
                toggleWiFi(false);
                MainActivity.this.handler.postDelayed(m_Runnable,(Integer.parseInt(offdata1))*1000);
            }
            else {
                toggleWiFi(true);
                Toast.makeText(getApplicationContext(),"wifi is  on",Toast.LENGTH_LONG).show();
                MainActivity.this.handler.postDelayed(m_Runnable,(Integer.parseInt(ondata1))*1000);
            }
        }

    };

    private boolean wificonnection() {
        WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        if (wifi.isWifiEnabled()) {
            return true;
//wifi is enabled
        }
        else
            return false;
    }
    public boolean toggleWiFi(boolean status) {
        WifiManager wifiManager = (WifiManager) this
                .getSystemService(Context.WIFI_SERVICE);
        if (status == true && !wifiManager.isWifiEnabled()) {
            wifiManager.setWifiEnabled(true);

        } else if (status == false && wifiManager.isWifiEnabled()) {
            wifiManager.setWifiEnabled(false);
        }
        return status;
    }
    private boolean isNetworkAvailable() {
        ConnectionDetector connectionDetector=new ConnectionDetector(getApplicationContext());
        Boolean connection=connectionDetector.isConnectingToInternet();
        return connection;
    }
    

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        toggleWiFi(true);
        handler.removeCallbacksAndMessages(m_Runnable);
        handler.removeCallbacks(m_Runnable);
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
